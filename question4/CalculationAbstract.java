package question4;

public abstract class CalculationAbstract {

    abstract void addOperation(double a, double b);

    abstract void subOperation(double a, double b);

    abstract void mulOperation(double a, double b);

    abstract void divOperation(double a, double b);

}
