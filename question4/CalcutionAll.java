package question4;

public class CalcutionAll extends CalculationAbstract {


    public CalcutionAll(double a, double b, String operator) {

        switch (operator) {
            case "+":
                addOperation(a, b);
                break;
            case "-":
                subOperation(a, b);
                break;
            case "*":
                mulOperation(a, b);
                break;
            case "/":
                divOperation(a, b);

        }
    }

    @Override
    void addOperation(double a, double b) {

        System.out.println(" The result is " + a + " + " + b + " = " + (a + b));

    }

    @Override
    void subOperation(double a, double b) {

        System.out.println(" The result is " + a + " - " + b + " = " + (a - b));
    }

    @Override
    void mulOperation(double a, double b) {

        System.out.println(" The result is " + a + " * " + b + " = " + (a * b));

    }

    @Override
    void divOperation(double a, double b) {

        System.out.println(" The result is " + a + " / " + b + " = " + (a / b));
    }

}