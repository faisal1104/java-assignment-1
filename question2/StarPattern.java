package question2;

public class StarPattern {

    public static void main(String[] args) {

        int n = 4;

        for (int i = 1; i <= n; i *= 2) {

            for (int j = 1; j <= i; j++) {
                System.out.print("*");
            }
            System.out.println();

        }

        for (int i = n / 2; i > 0; i /= 2) {

            for (int j = 1; j <= i; j++) {
                System.out.print("*");
            }
            System.out.println();


        }


    }

}
