package question3;

public class StudentMarks {

    private int marks;

    public StudentMarks(int marks) {
        this.marks = marks;
    }

    public int getMarks() {
        return marks;
    }

}
