package question3;

import java.util.function.DoubleToIntFunction;

public class StudentInfo extends StudentMarks {

    public String name;
    public int roll;
    public static String varsityName;


    public static void staticBlock() {
        System.out.println("<Static method>\nYou are searching the information of a student of IIUC\n");
    }

    {
        System.out.println("<non Static block>\nThe information of this student is :");
    }

    public StudentInfo(String name, int roll, int mark) {
        super(mark);
        this.name = name;
        this.roll = roll;

        this.varsityName = "IIUC";

    }

    static {
        int year = 1990;
        System.out.println("<Static Block>\nIIUC was stablished in " + year + ".And it's a populer private university in Chittagong.\n");
    }


    public String toString() {
        return "name= " + name + ", roll=" + roll + ", Marks=" + getMarks() + ", Varsity Name= " + varsityName + "\n";
    }


}