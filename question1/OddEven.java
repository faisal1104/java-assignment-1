package question1;


import java.util.Scanner;

public class OddEven {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        System.out.print("Enter Starting number: ");
        int startNumber = scan.nextInt();
        System.out.print("Enter Ending number: ");
        int lastNumber = scan.nextInt();

        if (startNumber > lastNumber) {
            System.out.println("Starting number cannot be greater than Last umber.");
        } else if (startNumber <= 0 || lastNumber <= 0) {
            System.out.println("Starting number & Last number must be positive.");
        } else {


            int i, countOdd = 0, countEven = 0, indexEven, j = 0;
            indexEven = ((lastNumber - startNumber) / 2) + 1;

            int[] evenNumber = new int[indexEven];

            for (i = startNumber; i <= lastNumber; i++) {
                if (i % 2 == 0) {
                    countEven++;
                    evenNumber[j] = i;
                    j++;

                } else {
                    countOdd++;
                }
            }

            System.out.println("Total even number count: " + countEven);
            System.out.println("Total odd number count: " + countOdd);

            System.out.print("The even numbers between " + startNumber + " and " + lastNumber + " are: ");

            for (int k = 0; k < j; k++) {
                System.out.print(evenNumber[k] + " ");
            }
        }
    }
}
